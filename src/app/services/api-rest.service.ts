import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiRestService {
  private REST_API_SERVER = 'https://api.openchargemap.io/v3/poi/';

  constructor(private httpClient: HttpClient) {}

  public testLoadOpenCharge() {
    return this.httpClient.get(
      this.REST_API_SERVER + '?output=json&countrycode=US&maxresults=10&key=03cae0b4-5073-4bca-bf9c-c53a76073069'
    );
  }
}
