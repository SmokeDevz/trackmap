import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapaGlobalComponent } from './mapa-global/mapa-global.component';
import { CommonModule } from '@angular/common';
import { ConfirmationService, SharedModule } from 'primeng/api';
import { PanelModule } from 'primeng/panel';
import { ChipsModule } from 'primeng/chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { ChartModule } from 'primeng/chart';

import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [AppComponent, MapaGlobalComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SidebarModule,
    DialogModule,
    ButtonModule,
    SharedModule,
    CommonModule,
    SidebarModule,
    FormsModule,
    ChartModule,
    HttpClientModule,
    ReactiveFormsModule,
    ChipsModule,
    PanelModule,
  ],
  providers: [ConfirmationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
