import { ApiRestService } from './../services/api-rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapa-global',
  templateUrl: './mapa-global.component.html',
  styleUrls: ['./mapa-global.component.css'],
})
export class MapaGlobalComponent implements OnInit {
  showSidebar: any;
  values: string[];

  visibleSidebar1;

  visibleSidebar2;

  visibleSidebar3;

  visibleSidebar4;

  visibleSidebar5;

  display: boolean = false;

  showDialog() {
    this.display = true;
  }

  data: any;

  constructor(private apiRest: ApiRestService) {
    this.showSidebar = false;
    this.values = ['123123', 'asdasda'];

    this.data = {
      datasets: [
        {
          data: [11, 16, 7, 3, 14],
          backgroundColor: [
            '#FF6384',
            '#4BC0C0',
            '#FFCE56',
            '#E7E9ED',
            '#36A2EB',
          ],
          color: '#ffffff',
          label: 'My dataset',
        },
      ],
      labels: ['Red', 'Green', 'Yellow', 'Grey', 'Blue'],
    };
  }

  ngOnInit() {
    this.apiRest.testLoadOpenCharge().subscribe((data: any) => {
      console.log('requestDemoApiOpenCharge');
      console.log(data);
    });
  }

  ShowSidebar() {
    this.showSidebar = !this.showSidebar;
  }
}
